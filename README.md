# HagRAT

## Description
This is the readme for "hagrat" (often styled as HagRAT or hagr4t), a barebone Remote Access Tool.  It provides users with reliable trojan functionality without using untested code from the internet.  The hagrat will remain a so-called "dropper", it will allow reliable connectivity and the ability upload and execute files that can do more.

## Goals
HagRAT is a remote access tool, it will connect a Win32 victim to our C&C.
To keep HagRAT small and simple, it should do a few things, but do them very well:
- Connectivity to C&C, with fallbacks and being **undetectable**
- Process spawning (cmd.exe)
- Upload/download files

HagRAT has two parts, the *CC* and the *client*.  
### CC
Written in PHP, provides "admin" access to the victims.
### Client
Written in Win32/C, is the main "trojan" to run on the victims computer.

More info is on the wiki, that's the most up to date and is a living document.
